#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    destuff.py

    Command-line utility to view and extract resources from stuff-file.

    Part of StuffTools: Python-library to work with stuff-files (EmbedFs 1.0).

    :copyright: (c) 2013 unregistered <black@tmnhy.su>
    :license: MIT, see LICENSE for more details
"""

import logging
import stufftools
import argparse


logging.basicConfig(filename='stuff.log', level=logging.INFO,
            format='%(asctime)s - %(levelname)s %(message)s')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Stuff tools')
    parser.add_argument('-f','--file_name',
        help='Name of stuff-file.',required=True)
    parser.add_argument('-a','--action', help='Action.',
        required=True, choices=['list', 'search', 'saveall', 'save'])
    parser.add_argument('-i','--index', help='Index of a resource.',
        type=int, required=False)
    parser.add_argument('-s','--substring', help='Substring for search.',
        required=False)
    parser.add_argument('-p','--path', help='Path to save resources.',
        required=False)


    args = parser.parse_args()

    st = stufftools.BaseStuff(args.file_name)

    if args.action == 'list':
        if not args.index:
            print st.list_all()
        else:
            print st.list_by_index(args.index-1)
    if args.action == 'search':
        if args.substring:
            print st.list_search(args.substring)
    if args.action == 'save':
        if args.index:
            if args.path:
                st.save_by_index(args.index-1, path=args.path)
            else:
                st.save_by_index(args.index-1)
    if args.action == 'saveall':
        if args.path:
            st.save_all(path=args.path)
        else:
            st.save_all()

