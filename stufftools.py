# -*- coding: utf-8 -*-
"""
    stufftools.py

    This module provides a python-library to view and
    extract resources from stuff-file (EmbedFs 1.0).

    Part of StuffTools: Python-library to work with stuff-files (EmbedFs 1.0).

    :copyright: (c) 2013 unregistered <black@tmnhy.su>
    :license: MIT, see LICENSE for more details
"""

import logging
import struct
import os


log = logging.getLogger(__name__)


class BaseStuff(object):
    """
    Class to read and extract resources from stuff-file.
    """
    def __init__(self, filename):
        """
        """
        self.__fh = None
        self.__header = None
        self.__res_list = None
        self.open(filename)

    def __del__(self):
        """
        """
        self.close()

    def open(self, name):
        """
        Open stuff-file.
        """
        if self.__fh:
            self.close()
        self.name = name
        self.__count = self.__read_header()

    def close(self):
        """
        Close stuff-file.
        """
        try:
            self.__fh.close()
            log.debug('Close resource file: %s' % self.name)
        except (IOError, ) as e:
            log.error(e)

    def __read_header(self):
        """
        Read header from stuff-file.
        """
        try:
            self.__fh = open(self.name, 'rb')
            self.__header = {}
            self.__res_list = []
            log.debug('Open resource stuff: %s' % self.name)
        except IOError as e:
            log.error(e)
            return

        try:
            res_files = struct.unpack('=l', self.__fh.read(4))
            print res_files
            log.debug('%d resources in stuff-file.', res_files[0])
        except struct.error as e:
            log.error(e)
            return

        for f in xrange(res_files[0]):
            try:
                res_len = struct.unpack('=l', self.__fh.read(4))
                name_len = struct.unpack('=l', self.__fh.read(4))
                res_name = self.__fh.read(name_len[0])
            except struct.error as e:
                log.error(e)
                return            
            self.__header[res_name] = {'size': res_len[0]}
            self.__res_list.append(res_name)
            self.__fh.read(1)

        pos = self.__fh.tell()

        for i in self.__res_list:
            self.__header[i]['offset'] = pos
            pos += self.__header[i]['size']

        log.info('Get %s resources from %s', len(self.__res_list), self.name)
        return len(self.__header)

    def count(self):
        """
        Returns the number of resources from stuff-file.
        """
        return self.__count if self.__count else 0

    def list(self):
        """
        Returns a list of resources from the stuff-file.
        """
        return self.__res_list

    def search(self, substring):
        """
        Search by entering a substring in the name of the resource.
        """
        result  = []
        if self.__header:
            for f in self.__header:
                if substring.upper() in f.upper():
                    result.append(self.__res_list.index(f))
        result.sort()
        log.debug('%s resources in search result for \"%s\".', len(result),
            substring)
        return result

    def __name_by_index(self, index):
        """
        Get resource name by index
        """
        try:
            return self.__res_list[index]
        except IndexError as e:
            log.error(e)
            return None

    def __index_by_name(self, name):
        """
        Get index of resource by name
        """
        try:
            return self.__res_list.index(name)
        except ValueError as e:
            log.error(e)
            return None

    def __size_by_index(self, index):
        """
        """
        return self.__header[self.__res_list[index]]['size']

    def __offset_by_index(self, index):
        """
        """
        return self.__header[self.__res_list[index]]['offset']

    def __save_resource(self, index, out_file):
        """
        Save resource to file.
        """
        if not self.__res_list:
            return

        try:
            f = open(out_file, 'wb')
        except IOError as e:
            log.debug(e)
            return

        name = self.__name_by_index(index)

        if self.__fh:
            self.__fh.seek(self.__header[name]['offset'])
            file_len = self.__header[name]['size']
            while (file_len > 0):
                buf = self.__fh.read(file_len)
                f.write(buf)
                file_len -= len(buf)
            f.close()
            log.info('Save resources \"%s\" to file \"%s\"', name, out_file)

    def save_by_index(self, index, path='./stuff'):
        """
        Save resource to file.
        """
        name = '/'.join([
            path,
            self.__name_by_index(index).replace('\\', '/')
        ])
        d = os.path.dirname(name)
        if not os.path.exists(d):
            os.makedirs(d)
        self.__save_resource(index, name)

    def save_all(self, path='./stuff'):
        """
        Save all resources to files.
        """
        for i in xrange(self.count()):
            name = '/'.join([
                path,
                self.__res_list[i].replace('\\', '/')
            ])
            d = os.path.dirname(name)
            if not os.path.exists(d):
                os.makedirs(d)
            self.__save_resource(i, name)

    def list_by_index(self, index):
        """
        Listing info about a resource by index.
        """
        size = self.__size_by_index(index)
        name = self.__name_by_index(index)
        offset = self.__offset_by_index(index)
        return '%7d %8X %10d %s' % (index+1, offset, size, name)

    def list_all(self):
        """
        Listing info about all resources.
        """
        return '\n'.join(['%s' % self.list_by_index(i)
            for i in xrange(self.count())])

    def list_search(self, substr):
        """
        Listing info about found resources.
        """
        return '\n'.join(['%s' % self.list_by_index(i)
            for i in self.search(substr)])